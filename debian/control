Source: comparelib
Section: ocaml
Priority: extra
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Hilko Bengen <bengen@debian.org>
Build-Depends:
 debhelper (>= 9),
 ocaml-nox (>= 4.00.0),
 ocaml-findlib (>= 1.3.2),
 camlp4,
 camlp4-extra,
 libtype-conv-camlp4-dev (>= 109.28.00),
 ocamlbuild,
 dh-ocaml (>= 0.9~)
Standards-Version: 3.9.6
Vcs-Browser: https://salsa.debian.org/ocaml-team/comparelib
Vcs-Git: https://salsa.debian.org/ocaml-team/comparelib.git
Homepage: http://janestreet.github.io/

Package: libcompare-camlp4-dev
Provides:
 libcompare-ocaml-dev,
 ${ocaml:Provides}
Architecture: any
Depends:
 ocaml-findlib,
 camlp4,
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Description: OCaml syntax extension for deriving "compare" functions automatically
 Pa_compare is a camlp4 syntax extension that derives comparison
 functions from type representations.
 .
 The scaffolded functions are usually much faster than OCaml's
 Pervasives.compare. Scaffolding functions also gives you more
 flexibilty by allowing you to override them for a specific type and
 more safety by making sure that you only compare comparable values.
